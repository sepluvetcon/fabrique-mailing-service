# Сервис уведомлений
## Установка
1. Склонировать репозиторий

С SSH:
````
git@gitlab.com:sepluvetcon/fabrique-mailing-service.git
````
C HTTPS:
````
https://gitlab.com/sepluvetcon/fabrique-mailing-service.git
````

2. Создать виртуальное окружение

````
python -m venv venv
````
3. Активировать окружение

````
source\venv\bin\activate
````

4. Открыть .evn и заполнить

5. Установка зависимостей

````
pip install -r requirements.txt
````

7. Создать и применить миграции в базу данных

````
python manage.py makemigrations
python manage.py migrate
````

8. Запустить сервер

````
python manage.py runserver
````

## Endpoints:

***

```.../api/``` - API Root

```.../api/clients/``` - Список клиентов

```.../api/mailings/``` - Список рассылки

```.../api/mailings/fullinfo/``` - Общая статистика по всем рассылкам

```.../api/mailings/<pk>/info/``` - Детальная статистика по конкретной рассылке

```.../api/messages/``` - Все сообщения

```.../docs/``` - Документация API

```...:6666``` - Celery & Flower

***

## Запуск celery
````
celery -A notification_service worker -l info
````
## Запуск flower

````
celery -A notification_service flower --port=6666
````

## Тесты
````
python manage.py test
```` 

## Список сделанных дополнительных заданий

<ol>
<li>организовать тестирование написанного кода</li> 
<li>сделать так, чтобы по адресу <i> /docs/ </i> открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: <a href="https://petstore.swagger.io" target="_blank">https://petstore.swagger.io</a></li>
<li>реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям</li>
<li>удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.</li>
<li>реализовать дополнительную бизнес-логику: добавить в сущность "рассылка" поле "временной интервал", в котором можно задать промежуток времени, в котором клиентам можно отправлять сообщения с учётом их локального времени. Не отправлять клиенту сообщение, если его локальное время не входит в указанный интервал.</li>
</ol>

