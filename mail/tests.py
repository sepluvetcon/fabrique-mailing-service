from django.test import TestCase
from django.utils.timezone import now
from rest_framework.test import APITestCase
from .models import Mailing, Client, Message


class TestModels(TestCase):

    def test_mailing_create(self):
        test_mailing = Mailing.objects.create(start_date=now(), end_date=now(), start_time=now().time(),
                                              end_time=now().time(), text='Hello there', tag='first')
        self.assertEqual(test_mailing.tag, 'first')
        self.assertNotEqual(test_mailing.text, 'Hi')
        self.assertIsInstance(test_mailing, Mailing)

    def test_client_create(self):
        test_client = Client.objects.create(phone_number='74955005550', mobile_operator_code='495', tag='sber',
                                            timezone='UTC')
        self.assertEqual(test_client.phone_number, '74955005550')
        self.assertEqual(test_client.mobile_operator_code, '495')
        self.assertNotEqual(test_client.tag, 'vtb')
        self.assertIsInstance(test_client, Client)

    def test_message_create(self):
        self.test_mailing_create()
        self.test_client_create()
        test_message = Message.objects.create(sending_status='No sent', mailing_id=3, client_id=3)
        self.assertIsInstance(test_message, Message)
        self.assertEqual(test_message.sending_status, 'No sent')


class TestAPI(APITestCase):

    def test_api_mailing(self):
        test_mailing_create = {"start_date": now(), "end_date": now(), "start_time": now().time(),
                               "end_time": now().time(), "text": "Hello there", "tag": "first",
                               "mobile_operator_code": '486'}
        response = self.client.post('http://127.0.0.1:8000/api/mailings/', test_mailing_create)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['text'], 'Hello there')
        self.assertIsInstance(response.data['text'], str)

    def test_client(self):
        test_client_create = {"phone_number": '73869876112', "tag": "first", "timezone": "UTC"}
        response = self.client.post('http://127.0.0.1:8000/api/clients/', test_client_create)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['phone_number'], '73869876112')
        self.assertIsInstance(response.data['phone_number'], str)

    def test_message(self):
        response = self.client.get('http://127.0.0.1:8000/api/messages/')
        self.assertEqual(response.status_code, 200)
